import React, { useState, useEffect } from 'react';

export default function CreateAppointment() {
    const [date, setdate] = useState("")

    const [time, settime] = useState("")

    const [vin, setvin] = useState("")

    const [customer, setcustomer] = useState("")

    const [technician, settechnician] = useState("")

    const [Techs, setTechs] = useState([])

    const [reason, setreason] = useState("")

    const fetchData = async () => {
      try {

        const response = await fetch('http://localhost:8080/api/technicians/');
        if (response.ok) {

          const data = await response.json();
          setTechs(data.Techs);
        } else {
          console.error('Failed to fetch technicians');
        }
      } catch (error) {
        console.error('Error fetching technicians:', error);
      }
    };

    useEffect(() => {
      fetchData();
    }, []);

    const handletechnicianChange = (event) => {

        const value = event.target.value
        settechnician(value)
    }

    const handlereasonChange = (event) => {

        const value = event.target.value
        setreason(value)
    }

    const handlecustomerChange = (event) => {

        const value = event.target.value
        setcustomer(value)
    }

    const handlevinChange = (event) => {

        const value = event.target.value
        setvin(value)
    }

    const handledataChange = (event) => {

        const value = event.target.value
        setdate(value)
    }

    const handletimeChange = (event) => {

        const value = event.target.value
        settime(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const date_time = date + " " + time

        const data = {}
        data.date_time = date_time
        data.vin = vin
        data.customer = customer
        data.technician = technician
        data.reason = reason

        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
            }
        }

        const response = await fetch("http://localhost:8080/api/appointments/", fetchConfig)
        if (response.ok){
            setdate("")
            settime("")
            setvin("")
            setcustomer("")
            settechnician("")
            setreason("")
        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
                <input onChange={handlevinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Automobile vin</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlecustomerChange} value={customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handledataChange} value={date} placeholder="date" required type="date" name="date" id="date" className="form-control"/>
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handletimeChange} value={time} placeholder="time" required type="time" name="time" id="time" className="form-control"/>
                <label htmlFor="time">Time</label>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handletechnicianChange} value={technician} required id="technician" name="technician" className="form-select">
                  <option value="">Technician</option>
                  {Techs.map(technician => {
                    return (
                        <option key ={technician.id} value={technician.first_name}>
                            {technician.first_name + " " + technician.last_name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlereasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <button className="btn btn-primary"> Create </button>
              </form>
          </div>
        </div>
      </div>
        </>
    )
    }
