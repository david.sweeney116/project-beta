import { useEffect, useState } from "react"

export default function UnsoldAutos() {
    const[unsold, setUnsold] = useState([])

    const fetchUnsoldData = async () => {
        const url = "http://localhost:8100/api/unsold/"
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setUnsold(data.unsold)
        }else{
            console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchUnsoldData();
       }, []);

       return (
        <>
        <div className="row">
        <h1>Unsold Inventory</h1>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Vehicle Identification Number</th>
            <th>Vehicle Color</th>
            <th>Vehicle Year</th>
            <th>Vehicle Manufacturer</th>
            <th>Vehicle Model</th>
          </tr>
        </thead>
        <tbody>
          {unsold.map(auto => {
            return (
              <tr key={ auto.id }>
                <td>{ auto.vin }</td>
                <td>{ auto.color }</td>
                <td>{ auto.year }</td>
                <td>{ auto.manufacturer }</td>
                <td>{ auto.model }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
}
