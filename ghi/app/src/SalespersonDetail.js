import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function SalespersonDetail() {
    const [SalespersonDetails, setSalespersonDetails] = useState([])
    const [sales, setSalepersonsales] = useState([])
    const {id} = useParams()

    useEffect(() => {
        const fetchData = async () => {
        const response = await fetch(`http://localhost:8090/api/salespeople/${id}`)

        if(response.ok) {
            const data = await response.json()
            setSalespersonDetails(data)
        }else{
            console.error("Bad Request. Maybe check the id?")
        }
    }
    fetchData()
    }, [id]);

    const fetchSaleData = async () => {
        const url = `http://localhost:8090/api/salespeople/${id}/sales`
        const response = await fetch(url)

        if (response.ok) {
            const data = await response.json()
            setSalepersonsales(data.sales)
        }else{
            console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchSaleData();
      }, []);

        return (
            <>
        <div className="text-center">
            <h1>List of Sales</h1>
        </div>
        <table className="table table-striped">
        <thead>
          <tr>
            <th>Price</th>
            <th>Customer</th>
            <th>Vehicle VIN Number</th>
          </tr>
        </thead>
        <tbody>
          {sales.map(sale => {
            return (
              <tr key={ sale.Automobile_Vin } value={sale.Automobile_Vin}>
                <td>${ sale.price }.00</td>
                <td>{ sale.Customer_First_Name + " " + sale.Customer_last_name }</td>
                <td>{ sale.Automobile_Vin }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
          </>
        )
}
