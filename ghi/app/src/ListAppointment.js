import React, { useEffect, useState } from 'react';

export default function ListAppointments() {
    const [Appointments, setAppointments] = useState([])

    const fetchData = async () => {

      const url = "http://localhost:8080/api/appointments/"

      const response = await fetch(url)
      if (response.ok) {

          const data = await response.json()

          const formattedAppointments = data.Appointments.map(appointment => {

            const dateTime = new Date(appointment.date_time);

                const date = dateTime.toDateString();

                const time = dateTime.toLocaleTimeString();

                return {
                    ...appointment,
                    date,
                    time,
                };
            });
            setAppointments(formattedAppointments);
        }
    }

    const markAppointmentAsFinished = async (id) => {

      const finishURL = `http://localhost:8080/api/appointments/${id}/finish/`;

      const response = await fetch(finishURL, {
          method: 'PUT',
      });
      if (response.ok) {
          setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== id));
      } else {
          console.error('Failed to mark appointment as finished.');
      }
  }

  const markAppointmentAsCanceled = async (id) => {

      const cancelURL = `http://localhost:8080/api/appointments/${id}/cancel/`;

      const response = await fetch(cancelURL, {
          method: 'PUT',
      });
      if (response.ok) {
          setAppointments(prevAppointments => prevAppointments.filter(appointment => appointment.id !== id));
      } else {
          console.error('Failed to mark appointment as canceled.');
      }
  }

    useEffect(() => {
        fetchData();
    }, []);

    return (
      <div>
        <div className="row">
          <h1>Appointments</h1>
        </div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Vin</th>
                <th>VIP?</th>
                <th>customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
              </tr>
            </thead>
            <tbody>
            {Appointments.map(Appointments => (
  (Appointments.status !== 'canceled' && Appointments.status !== 'finished') ? (
      <tr key={Appointments.id}>
          <td>{Appointments.vin}</td>
          <td>{Appointments.VIP ? 'Yes' : 'No'}</td>
          <td>{Appointments.customer}</td>
          <td>{Appointments.date}</td>
          <td>{Appointments.time}</td>
          <td>{Appointments.technician.first_name + " " + Appointments.technician.last_name}</td>
          <td>{Appointments.reason}</td>
          <td>{Appointments.status}</td>
          <td>
              {Appointments.status !== 'finished' && (
                  <button onClick={() => markAppointmentAsFinished(Appointments.id)}>Finished</button>
              )}
              {Appointments.status !== 'canceled' && (
                  <button onClick={() => markAppointmentAsCanceled(Appointments.id)}>Canceled</button>
              )}
          </td>
      </tr>
  ) : null
))}
</tbody>
</table>
</div>
);
}
