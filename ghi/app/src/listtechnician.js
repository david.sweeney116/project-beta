import React, { useEffect, useState } from 'react';

export default function Listtechnician() {
    const [Techs, setTechnicians] = useState([])

    const fetchData = async () => {
        const url = "http://localhost:8080/api/technicians/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setTechnicians(data.Techs )
        }
    }

    useEffect(() => {
        fetchData();
       }, []);

       return (
        <>
        <div className="row">
        <h1>Technicians</h1>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Employee ID</th>
            <th>First Name</th>
            <th>Last Name</th>
          </tr>
        </thead>
        <tbody>
          {Techs.map(Techs => {
            return (
              <tr key={ Techs.id }>
                <td>{ Techs.employee_id }</td>
                <td>{ Techs.first_name }</td>
                <td>{ Techs.last_name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
}
