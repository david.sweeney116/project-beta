import { useEffect, useState } from "react";

export default function ListManufacturers() {
    const [manufacturers, setManufacturers] = useState([])

    const fetchManufacturerData = async () => {
        const url = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setManufacturers(data.manufacturers)
        }else{
            console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchManufacturerData();
       }, []);

       return (
        <>
        <div className="row">
        <h1>Manufacturers</h1>
        </div >
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map(manufacturer => {
            return (
              <tr key={ manufacturer.id }>
                <td>{ manufacturer.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
}
