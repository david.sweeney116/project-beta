import { useEffect, useState } from "react";

export default function CreateAutomobile() {
    const [color, setColor] = useState("")
    const [year, setYear] = useState("")
    const [vin, setVin] = useState("")
    const [model_id, setModel] = useState("")
    const [models, setModels] = useState([])

    const fetchModelData = async () => {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url)

        if(response.ok){
            const data = await response.json()
            setModels(data.models)
        }else{
            console.error("Bad request")
        }
    }
    
    useEffect(() => {
        fetchModelData();
       }, []);

    const handleVinChange = (event) => {
        const value = event.target.value
        setVin(value)
    }

    const handleColorChange = (event) => {
        const value = event.target.value
        setColor(value)
    }

    const handleYearChange = (event) => {
        const value = event.target.value
        setYear(value)
    }

    const handleModelChange = (event) => {
        const value = event.target.value
        setModel(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}
        data.color = color
        data.year = year
        data.vin = vin
        data.model_id = model_id
        const url = "http://localhost:8100/api/automobiles/"
        const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json"
            }
        }
        const response = await fetch(url, fetchConfig)

        if(response.ok){
            setColor("");
            setYear("");
            setVin("");
            setModel("")
        }else{
            console.error("Bad Response")
        }
    }

    return (
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Submit a new Automobile</h1>
            <form onSubmit={handleSubmit} id="create-salesperson-form">
              <div className="form-floating mb-3">
                <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" name="year" id="year" className="form-control"/>
                <label htmlFor="year">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="mb-3">
                <select onChange={handleModelChange} value={model_id} required id="model" name="model" className="form-select">
                  <option value="">Choose a Model</option>
                  {models.map(model => {
                    return (
                        <option key ={model.id} value={model.id}>
                            {model.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Submit</button>
              </form>
          </div>
        </div>
      </div>
        </>
    )
}
