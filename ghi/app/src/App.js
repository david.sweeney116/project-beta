import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListSalespeople from './ListSalespeople'
import CreateSalesperson from './CreateSalesperson'
import ListCustomers from './ListCustomers';
import CreateCustomer from './CreateCustomer';
import RecordSale from './RecordSale';
import ListSales from './ListSales';
import SalespersonHistory from './SalespersonHistory';
import SalespersonDetail from './SalespersonDetail';
import ListManufacturers from './ListManufacturers';
import CreateManufacturer from './CreateManufacturer';
import ListModels from './ListModels';
import CreateModel from './CreateModel';
import ListAutomobiles from './ListAutomobiles';
import CreateAutomobile from './CreateAutomobile';
import CreateAppointment from './Createappointment';
import ListAppointments from './ListAppointment';
import CreateTechnician from './Addtechnician';
import Listtechnician from './listtechnician';
import Servicehistory from './Servicehistory';
import UnsoldAutos from './UnsoldAuto';

export default function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />}/>
          <Route path="/listmanufacturers" element={<ListManufacturers/>}/>
          <Route path="/createmanufacturer" element={<CreateManufacturer/>}/>
          <Route path="/listmodels" element={<ListModels/>}/>
          <Route path="/createmodel" element={<CreateModel/>}/>
          <Route path="/createautomobile" element={<CreateAutomobile/>}/>
          <Route path="/listautomobiles" element={<ListAutomobiles/>}/>
          <Route path="/unsoldinventory" element={<UnsoldAutos/>}/>
          <Route path="/customers" element={<ListCustomers/>}/>
          <Route path="/salespeople" element={<ListSalespeople/>}/>
          <Route path="/createsalesperson" element={<CreateSalesperson/>}/>
          <Route path="/createcustomer" element={<CreateCustomer/>}/>
          <Route path="/createsale" element={<RecordSale/>}/>
          <Route path="/listsales" element={<ListSales/>}/>
          <Route path="/Createappointment" element={<CreateAppointment/>}/>
          <Route path="/Addtechnician" element={<CreateTechnician/>}/>
          <Route path="/ListAppointment" element={<ListAppointments/>}/>
          <Route path="/listtechnician" element={<Listtechnician/>}/>
          <Route path="/Servicehistory" element={<Servicehistory/>}/>
          <Route path="/salespersonhistory" element={<SalespersonHistory/>}>
            <Route path=":id" element={<SalespersonDetail/>}/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}
