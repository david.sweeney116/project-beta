import { useEffect, useState } from "react";

export default function ListModels() {
    const [models, setModels] = useState([])

    const fetchModelData = async () => {
        const url = "http://localhost:8100/api/models/"
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json()
            setModels(data.models)
        }else{
            console.error("Bad Request")
        }
    }

    useEffect(() => {
        fetchModelData();
        }, []);

    return (
        <>
        <div className="row">
        <h1>Models</h1>
        </div >
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Model Name</th>
            <th>Manufacturer</th>
            <th>Model Image</th>
            </tr>
        </thead>
        <tbody>
            {models.map(model => {
            return (
                <tr key={ model.id }>
                <td>{ model.name }</td>
                <td>{ model.manufacturer.name }</td>
                <td><img src={model.picture_url} className="img-thumbnail rounded" alt="..."></img></td>
                </tr>
            );
            })}
        </tbody>
        </table>
        </>
    );
    }
