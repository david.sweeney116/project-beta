from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
import json
from .encoders import TechnicianListEncoder, AppointmentlistEncoder
from django.shortcuts import get_object_or_404


@require_http_methods(["GET", "POST"])
def TechnicianPollView(request):
    if request.method == "GET":
        Techs = Technician.objects.all()
        return JsonResponse(
            {"Techs": Techs},
            encoder=TechnicianListEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            Techs = Technician.objects.create(**content)
            return JsonResponse(
                Techs,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vins ID"},
                status=400,
            )


@require_http_methods(["DELETE"])
def TechnicianPolldelete(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Technician.DoesNotExist:
            return HttpResponse(status=400)


@require_http_methods(["GET", "POST"])
def AppointmentPollView(request):
    if request.method == "GET":
        Appointments = Appointment.objects.all()
        return JsonResponse(
            {"Appointments": Appointments},
            encoder=AppointmentlistEncoder
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid vins ID"},
                status=400,
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentlistEncoder,
            safe=False
        )


@require_http_methods(["DELETE"])
def AppointmentsPolldelete(request, id):
    if request.method == "DELETE":
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return HttpResponse(status=400)


@require_http_methods(["PUT"])
def AppointmentsPollcanceled(request, id):
    if request.method == 'PUT':
        appointment = get_object_or_404(Appointment, id=id)
        appointment.status = "Canceled"
        appointment.save()
        return JsonResponse({"message": "Appointment status updated to 'Canceled'."})
    return JsonResponse({"message": "Unsupported HTTP method"}, status=405)


@require_http_methods(["PUT"])
def AppointmentsPollfinished(request, id):
    if request.method == 'PUT':
        appointment = get_object_or_404(Appointment, id=id)
        appointment.status = "Finished"
        appointment.save()
        return JsonResponse({"message": "Appointment status updated to 'Finished'."})
    return JsonResponse({"message": "Unsupported HTTP method"}, status=405)
