from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold"]


class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["first_name", "last_name", "employee_id", "id"]


class AppointmentlistEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "status",
        "vin",
        "customer",
        "technician",
        "reason",
        "id"
    ]
    encoders = {
        "technician":TechnicianListEncoder(),
    }
