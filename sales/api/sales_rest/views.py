from django.shortcuts import render

from django.views.decorators.http import require_http_methods

from django.http import JsonResponse, HttpResponse

from .encoders import SalespersonEncoder, CustomerModelEncoder, SaleModelEncoder

from .models import SalespersonModel, AutomobileVO, CustomerModel, SaleModel

import json


@require_http_methods(["GET", "POST"])
def api_list_salesperson(request):
    if request.method == "GET":
        try:
            salesperson = SalespersonModel.objects.all()
            return JsonResponse(
                {"salesperson": salesperson},
                encoder=SalespersonEncoder
            )
        except SalespersonModel.DoesNotExist:
            return JsonResponse(
                {"message": "Bad request"},
                status=400,
            )
    else:
        content = json.loads(request.body)
        try:
            salesperson = SalespersonModel.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except SalespersonModel.DoesNotExist:
            return JsonResponse(
                {"message": "Bad request"},
                status=400,
            )


@require_http_methods(["DELETE", "GET"])
def api_detail_salesperson(request, id):
    if request.method == "DELETE":
        count, _ = SalespersonModel.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"message": "salesperson successfully deleted"})
        else:
            return JsonResponse(
                {"message": "salesperson ID does not exist"},
                status=400
                )
    else:
        salesperson = SalespersonModel.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_salesperson_sales(request, id):
    if request.method == "GET":
        sales = SaleModel.objects.all()
        response = []
        for sale in sales:
            if sale.salesperson.id == id:
                response.append({
                    "price": sale.price,
                    "Automobile_Vin": sale.automobile.vin,
                    "Customer_First_Name": sale.customer.first_name,
                    "Customer_last_name": sale.customer.last_name
                })
        return JsonResponse({"sales": response})


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        try:
            customers = CustomerModel.objects.all()
            return JsonResponse(
                {"customers": customers},
                encoder=CustomerModelEncoder,
            )
        except CustomerModel.DoesNotExist:
            return JsonResponse(
                {"message": "Bad request"},
                status=400
            )
    else:
        content = json.loads(request.body)
        try:
            customer = CustomerModel.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerModelEncoder,
                safe=False
            )
        except CustomerModel.DoesNotExist:
            return JsonResponse(
                {"message": "Bad request"},
                status=400
            )


@require_http_methods(["DELETE"])
def api_delete_customer(request, id):
    if request.method == "DELETE":
        count, _ = CustomerModel.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"message": "Customer deleted successfully"},
                                status=200
                                )
        else:
            return JsonResponse(
                {"message": "Customer Id does not exist"},
                status=400)


@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        try:
            sales = SaleModel.objects.all()
            return JsonResponse(
                {"sales": sales},
                encoder=SaleModelEncoder,
            )
        except SaleModel.DoesNotExist:
            return JsonResponse(
                {"message": "Bad request"},
                status=400
            )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            salesperson = SalespersonModel.objects.get(employee_id=content["salesperson"])
            customer = CustomerModel.objects.get(first_name=content["customer"])
            content["automobile"] = automobile
            content["salesperson"] = salesperson
            content["customer"] = customer
        except AutomobileVO.DoesNotExist or SalespersonModel.DoesNotExist or CustomerModel.DoesNotExist:
            return JsonResponse(
                {"message": "bad request"},
                status=400
            )
        sale = SaleModel.objects.create(**content)
        sale.automobile.sold = True
        sale.save
        return JsonResponse(
            sale,
            encoder=SaleModelEncoder,
            safe=False
        )


@require_http_methods(["DELETE", "GET"])
def api_delete_sale(request, id):
    if request.method == "DELETE":
        count, _ = SaleModel.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"message": "sale was successfully deleted"},
                                status=200)
        else:
            return JsonResponse(
                {"message": "Sale ID does not exist"},
                status=400
            )
    else:
        sale = SaleModel.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleModelEncoder,
            safe=False
        )
